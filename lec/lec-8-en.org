#+INCLUDE: "common/org-header.org"
#+LANGUAGE: en
#+TITLE: Chapter 8. Programming languages in systems

So far, we have been studying the lowest levels of the system: hardware, virtualized hardware, structured executable, and relocatable object files.
They enable us to build an assembly program from modules, interface these modules, and execute every program in isolation.
But this is not enough to build complex systems because assembly language is not very well suited for their construction: it is verbose, unsafe, and it does not allow expressing complex algorithms concisely.
It is also hard to make an optimizer that would efficiently speed up an assembly program --- not because assembly programs are fast by themselves, but because they are extremely hard to analyze.
Giving programmers more control over the execution makes automating program optimization much harder, if even possible.
Therefore researchers and engineers came up with an idea of higher-level languages to conceive more complex systems.

If we want to understand, what is the role of programming languages in computer systems, we need to start with fundamental questions:

- What is a programming language?
- How to define a programming language?
- What is the meaning of programs?



* Defining a programming language

The assembly language bears no resemblance to natural languages like English.
It is an optimized code that computers can efficiently execute.
To formulate our ideas of computations in complex computer systems, we need a better language.
This may be some language closer to the natural language, which we routinely use to think and communicate with others.
However, such language should be well fit to describe computations.

** Requirements for programming language

In technical contexts such as writing documentation, programs, mathematical proofs, we need to transmit information in a way that is:

1) easy to read and write;
2) unambiguous;
3) easy to learn, described by a small number of simple rules.

Natural language is not suitable for writing programs:

1) It has not evolved to describe computations effectively, so it does not have a lot of concise, minimal idioms for that.
2) It is ambiguous, which makes interpreting some sentences difficult.
3) It is rich, so difficult to describe.

Before programmers, mathematicians have had their battle against the ambiguity and generality of natural languages:

1) In ancient times, mathematicians reasoned in natural language.
2) Then mathematicians have invented a dialect to talk about mathematics with idioms like "if and only if", "without loss of generality" etc.
3) Then a synthetic language of formal logic emerged and allowed to describe mathematics unambiguously and concisely.
  This language itself was based on simple rules so it is easy to learn or even automate the reasoning process in it.
  
Therefore we need simple synthetic languages specifically crafted for describing computations.
They should be concise, constructed through defining basic atoms and combining them in a formalized unambiguous way like the language of logic formulae.

To define a programming language, we need to describe its two aspects.

1) We know that each program is a sequence of characters but how do we separate valid programs from other strings?
2) What is the meaning of programs? How to relate the source code to the computations?

** Compiler as a language description

One way of defining a programming language to say that the compiler or interpreter itself is a complete, ultimate language definition, since it parses programs and either executes them or transforms them into executable code.
We can run this code and observe its effects on the machine state.

Using compilers or interpreters to define languages has serious issues:

1) It is hard to tell programs from other strings:
   1) The compiler bugs affect the language definition.
      If a program is not accepted by the compiler/interpreter, either the program is invalid, or the compiler contains an error. 
      Moreover, when the compiler changes e.g. when bugs get fixed, the set of "correct" programs changes with it.
   2) It is hard to write other compilers, analyzers, pretty printers, syntax highlighters, integrate the language in editors, and do other tasks relying on the knowledge of the language.
      
2) It is hard to understand the meaning of programs.
   1) What if a program can work in a multitude of ways e.g. due to parallelism? The execution of the program will differ each time we launch it.
   2) Compilers generate machine instructions with optimizations.
      The result is extremely hard to read and interpret: there are no functions nor variables, instructions operate only with memory and registers, performing assignments and arithmetic operations.
      Machine code is also processor-specific, so compiling for e.g. Intel 64 and ARM will produce incompatible and incomparable code.
      
Instead of defining a language by crafting compilers or interpreters for it, we need another, implementation-independent approach.
   
** Three aspects of languages

To define a language in a cleaner and implementation-independent way, we need to describe three aspects of language:
 
- Syntax :: the rules of statement constructions.
   It allows us to construct programs, check the correctness of their construction (but not the correctness of their /meaning/) and 
   We often use formal grammars to describe the language syntax.
- Semantics :: the meaning of language constructions.
  We will define the effect of each construction on the abstract machine of the language, an imaginary computer which is capable of executing the source code directly.
- Pragmatics :: an aspect that appears when we translate code to a different language; it describes how the meaning of program is changed by translation.

  In this chapter we study these three aspects in more details.
 

* Syntax

Mathematically speaking, a language is a /set/ of strings.
Every element of this set is called a /sentence/ of this language.

Formal languages have been born in mathematics, so it is natural to start with an example from basic mathematics.

Consider all arithmetic formulae with integer numbers and basic arithmetic: addition, subtraction, multiplication and division.
Here are some sentences of this language:

$$42$$

$$1 + 342$$

$$9 \times 3 + 7 / 42\dots$$

Sentences of this language are constructed from a limited set of symbols[fn:1], that we will denote as $\Sigma$:

$$\Sigma = \{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, + , -, \times, / \}$$

This set of symbols is called an /alphabet/ and is usually denoted as $\Sigma$.
Alphabet is a base of any language.
Different alphabets produce different languages, but one alphabet is a base for many different languages.

Consider a set $\Sigma^*$ containing all possible combinations of symbols from the alphabet[fn:2] $\Sigma$.

$$\Sigma^*= \{ 42321-, ++, 9+7 \dots \}$$

The string $9+7$ is a valid formula, and $++$ is not.
So, $\Sigma^*$ contains both valid and invalid formulae, and an alphabet is not enough to define the language of arithmetic formulae.
This language of formulae will be a subset of $\Sigma^*$.

How to distinguish valid language sentences from other members of $\Sigma^*$?
Since the language is infinite, it is impossible to enumerate all correct sentences.
One way of building a fence around the language is to define its structure, identifying its basic sentences and constructions to compose them into more complex sentences.
Then, if there is a way to combine basic constructions into a given sentence, this sentence is a correct language statement.

For example, we may identify the following combinations of basic elements:

- a /digit/ is any character in the range $0\dots9$;
- a /number/ is a sequence of digits;
- an /operation/ is either +, -, \times or /
- an /expression/ is either a number or two expressions joined by an operation.

/Formal grammars/ are a way of describing such rules of composition. We will study them in the next section.

  
** Grammars


American linguist Noam Chomsky has introduced formal grammars in his research of natural languages.
/Formal grammars/ describe sentences with a tree-like structure, where:

- leaves are primitive, atomic blocks;
- complex parts are composed out of other parts, primitive or not, according to specific rules.
  
Both primitive and composite parts are called /symbols/; the primitive symbols are called /terminal symbols/, or /terminals/, and the composed symbols are called /non-terminal symbols/, or /non-terminals/.

Formally, grammar consists of:

- A finite set of terminal symbols.
- A finite set of non-terminal symbols.
- A finite set of production rules, which describe the language structure.

In the language of arithmetic formulae:

- Terminal symbols match the characters of the alphabet exactly:

   $$\Sigma =\text{terminals} = \{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, + , -, \times, / \}$$

  This is not the case for every language: for example, the alphabet of programming languages contains letters, spaces, digits etc. but the set of terminals contains whole keywords such as =if=, =while=, or =void=.
  
- Non-terminal symbols are /digit/, /number/, /operation/ and /expression/. 

  One non-terminal is selected as a /starting symbol/; this non-terminal corresponds to any valid sentence.

- Some of production rules are (symbols are denoted with <brackets>):

#+begin_example
<digit>  ::= "0"
<digit>  ::= "1"
<digit>  ::= "2"
...
<digit>  ::= "9"
<number> ::= <digit> <number>
<number> ::= <digit>
<operation> ::= "+"
<expression> ::= <expression> <operation> <expression>
#+end_example

As we see, every rule describes a possible structure of a non-terminal.
We are only interested in such grammars where rules are of the following form:

#+begin_example
nonterminal ::= sequence of terminals and nonterminals
#+end_example

If there are multiple rules for one nonterminal, they are all equally valid.
To make it less verbose, we will use the notation with the symbol =|= to denote /or/, just like in regular expressions.
#+begin_example
<digit>  ::= "0" | "1" | ... | "9"
<number> ::= <digit> <number> | <digit>
<operation> ::= "+" | ...
<expression> ::= <expression> <operation> <expression> | <number>
#+end_example

It is trivial to transform rules with =|= into a simpler form, so the addition of =|= serves convenience but does not make grammars more powerful than they already are.
This way of describing grammar rules is called /EBNF (Extended Backus-Naur form)/.

Sometimes it is convenient to introduce a terminal $\epsilon$, which corresponds to an empty (sub)string.

** Problems solved by grammars

Formal grammars allow us to construct synthetic languages with very simple structure.
The simplicity is, of course, relative to the natural languages.
Formal grammars also act as a model of computation and are effective in solving the following problems:

- Find out whether a string of characters is a correct language statement. For example, is =5+39*7*9/2+3343= a valid arithmetic formula?

- Generate correct language statements.
  For example, enumerate all arithmetic formulae smaller than 1000 characters long.
    
- Parse language statements into hierarchical structures reflecting the statement structure.
  For example, an expression =2*5= may be represented as a tree where multiplication is the root, and =2= and =5= are its children:
  
#+INCLUDE: "common/before-image.org"
#+attr_html: :width 30%
#+attr_org: :width 0.30
#+attr_latex: :width 0.30\textwidth
[[./img/ast-mul.svg]]
#+INCLUDE: "common/after-image.org"

  A more complex expression such as =4+7*9= will have a deeper structure, like this:
  
#+INCLUDE: "common/before-image.org"
#+attr_html: :width 30%
#+attr_org: :width 0.30
#+attr_latex: :width 0.30\textwidth
  [[./img/ast-add-mul.svg]]
#+INCLUDE: "common/after-image.org"

It is easier to work with such tree-like structures than with plain text; they are called /abstract syntax trees/.

 
** Abstract Syntax Trees


** Chomsky's hierarchy

# [
#+INCLUDE: "common/before-image.org"
#+attr_latex: :width 0.60\textwidth
#+attr_html: :width 60%
{{{if-latex-else([[./img/chomsky-hierarchy.svg]],)}}}
#+INCLUDE: "common/after-image.org"
# ]


# <
#+BEGIN_EXPORT md
<p align='center'> <img width='90%'  src='./img/./img/chomsky-hierarchy.svg ' /> </p>
#+END_EXPORT
# >

** Types

Make most errors syntactic.
Decomposition -> composition, but limit it.

** Syntax and composition in systems


#+begin_comment
In programming languages, syntax defines basic rules of composition through:
   - syntactic categories (numbers, expressions, statements) and the rules of their combinations (=x = <rhs>= expects expression on the =<rhs>=, not a statement).
   - data types, which limit composition (not every expression will do for =x = <rhs>=, only one of the type matching the type of =x=).



                    \item In some situations, the language standard does not
                        provide enough information about the program behavior.
                        Then it is entirely up to compiler to decide, how will
                        it translate this program, so it is often assigning
                        some specific behavior to such programs. 

                        For example, in the call \c{f(g(x), h(x))} the 
                        order of evaluation of \c{g(x)} and \c{h(x)} is
                        not defined by standard. We can either compute
                        \c{g(x)} and then \c{h(x)}, or vice versa. But the
                        compiler will pick a certain order and generate 
                        instructions that will perform calls in exactly this
                        order.

                        \item Sometimes there are different ways of translating
                            the language constructions into the target code. 
                            For example, do we want to prohibit the compiler
                            from inlining certain functions, or do we 
                            stick with laissez-faire strategy?

#+end_comment

* Semantics
   Semantics of a programming language can be described in several ways.
 Even in natural language your sentence may be well constructed but be absurd and devoid of meaning.
 This is true for formal languages as well: some sentences may be /syntactically correct/ but meaningless.
 For example, the formula =9+23/0= is syntactically correct, but we are unable to compute it because of division by zero.


#+begin_comment
Operational, denotational, axiomatic
#+end_comment

The most straightforward way is called /operational semantics/; it defines the action of all language constructions on the state of the abstract machine.
An action is matched to the AST node, 
For example, in C, an assignment will result in a write to the memory of an abstract machine, and calling a function will make it switch to executing different part of code, to return to the calling place 

*** Behavior
/Behaviour/ is the observable outcome of the program functioning.
For C the language we may define program behaviour as a sequence of the following events:

- Reads and writes to volatile variables.
- Calls to external functions and system calls.

A C program can have zero, one, several behaviours or demonstrate any behaviour imaginable.

*** Determinism and non-determinism
*** Totality and undefinedness
*** Sequence points

** Pragmatics
*** Translating semantics
*** Memory layout details
    Stack
    Alignment
    Padding
    Strict aliasing rules
** Optimizations
   Sample optimizations:
*** Precomputation
    function inlining etc
*** Copy elision
*** ???  
*** Reorderings

* Memory models
Reduce program semantics to reads and writes
What can be reordered?
   
* System levels 

Layering is one of the common approaches to structuring systems.

It is easier to build a complex system layer by layer.

Each layer has:
- Structural units.
- Their interfaces.
- Rules of composition.
- Operational aspect: what is the meaning of the composition of the

Each layer is related to the adjacent layers.
Translation to the lower level.
Base for the next level.


** Systemic aspects of programming languages
1) Language constructions, syntax.
   Syntax defines rules of composition on the highest levels, through:
   - syntactic categories (expressions, statements) and the rules of their combinations (=x = <rhs>= expects expression on the =<rhs>=, not a statement).
   - types that limit composition (not every expression will do for =x = <rhs>=, only one of the type matching the type of =x=).

   Abstract machine. Memory model.
   Program semantics, program behaviour; undefined, unspecified, implementation defined.

2) Modules.
   Every module is a half-baked program with pending connections to other modules.
   Code semantics is the same as assembly.
   Linking is a composition through names; relocation.
   
   Between 1 and 2: compilation, memory model translation.

   
3) Processes.
   Thread, a virtual CPU.
   Machine code.
   Address space, virtual memory.

   Composition through shared memory, pipes, files, system calls etc.

   Between 2 and 3: linking, relocation, loading.

4) Physical CPU, memory and storage.

   Composition through interfaces between CPUs (if multiple).

   Machine code.

   Between 3 and 4: virtualization through multiplexing and aggregation (virtual memory) and multiplexing (CPU).
 
* Overview

* Footnotes
[fn:2] Generally, given a set $S$, $S^*$ means constructing a set of all possible combinations of elements in $S$. This includes combinations of all lengths including a zero-length empty sequence.

[fn:1] Formulae also contain parentheses and spaces, but we will omit this detail to simplify the language.
